Team:
Jesse McKinley
Ryan Sobczak

URL to Presentation:
https://docs.google.com/presentation/d/1z3JoRPGULYnnXJl614dvvKNw-vN34g6wQGg1Xkvsb6U/edit?usp=sharing

URL to the Repo:
https://bitbucket.org/ryansobczak/egp405_a2_01_mckinley_jesse_sobczak_ryan

-IMPORTANT-
Starting the Game:
-Build the Server first to create the dll files into the same folder as the .exe programs

To connect the client to the server type in:
"Client.exe ClientPort ServerIP ServerPort"
To init the server type in:
"Server.exe ServerPort"

Game Description:
This is a pongbreak game that uses networking and 2D graphics.
The networking is done using RakNet
The 2D graphics is done using SFML

Controls:
-Chat Controls-
Type: to chat 
Enter: to send the chat message to the other person

-Game Controls-
Move the mouse up: move paddle up
MOve the mouse down: move the paddle down

Unique Features:
Chat capibilities
Robust physics system
Can connect a player to another one, when one player disconnects