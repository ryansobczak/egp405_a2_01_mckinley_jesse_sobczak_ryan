#include "client.h"
#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "Gets.h"
#include "Kbhit.h"

#include <iostream>

Client::Client()
{
	mpRenderWindow = nullptr;
	mpEvent = nullptr;

	mpPhysics = new PhysicsObjectManager;
	CurrentTime = 0;
}

Client::~Client()
{
	cleanup();
}

void Client::cleanup()
{
	if (mpRenderWindow != nullptr)
	{
		delete mpRenderWindow;
		mpRenderWindow = nullptr;
	}
	if (mpEvent != nullptr)
	{
		delete mpEvent;
		mpEvent = nullptr;
	}
	mPlayer1Pad = NULL;
	mPlayer2Pad = NULL;
	mBall = NULL;
}

unsigned char Client::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

bool Client::LaunchClient(RakNet::RakPeerInterface* a_client, int a_clientPort, const char* a_serverIP, int a_serverPort, bool isCommandLine)
{
	mIsCommandLine = isCommandLine;
	CurrentTime = 0;
	// IPV4 socket
	RakNet::SocketDescriptor sd(a_clientPort, 0);
	sd.socketFamily = AF_INET;

	int startupResult = a_client->Startup(1, &sd, 1);
	a_client->SetOccasionalPing(true);

	if (a_client->Connect(a_serverIP, a_serverPort, 0, 0) !=
		RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		printf("Attempt to connect to server FAILED\n");
		return false;
	}

	printf("\nCLIENT IP addresses:\n");
	for (unsigned int i = 0; i < a_client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, a_client->GetLocalIP(i));
	}

	return true;
}

bool Client::HandlePackets(RakNet::RakPeerInterface* a_peer)
{
	if (_kbhit())
	{
		//chat messages
		chatMessage chat2;
		Gets(mChat.message, sizeof(mChat.message));
		std::string prefix = "Player" + std::to_string(mPlayerID) + ": ";

		strncpy_s(chat2.message, prefix.c_str(), sizeof(chat2.message));
		strncat_s(chat2.message, mChat.message, sizeof(chat2.message) - strlen(prefix.c_str()) - 1);

		if (sizeof(mChat.message) > 1)
		{
			chat2.packetID = (unsigned char)messages::ID_TTT_CHAT;
			a_peer->Send((const char*)&(chat2), sizeof(chat2), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		}
	}

	////player input
	//if (mpRenderWindow != nullptr)
	//{
	//	while (mpRenderWindow->getWindow().pollEvent(*mpEvent))
	//	{
	//		if (mpEvent->type == sf::Event::KeyPressed)
	//		{
	//			switch (mpEvent->key.code)
	//			{
	//				case sf::Keyboard::Up:
	//					//printf("up\n");
	//					//mMovement.keyPressed = 1;
	//					//mMovement.playerNum = mPlayerID;
	//					//mMovement.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
	//					//a_peer->Send((const char*)&(mMovement), sizeof(mMovement), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	//					break;

	//				case sf::Keyboard::Down:
	//					//printf("down\n");
	//					//mMovement.keyPressed = -1;
	//					//mMovement.playerNum = mPlayerID;
	//					//mMovement.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
	//					//a_peer->Send((const char*)&(mMovement), sizeof(mMovement), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	//					break;

	//				default:
	//					break;
	//			}
	//		}
	//	}
	//}

	for (RakNet::Packet* p = a_peer->Receive();	p; a_peer->DeallocatePacket(p), p = a_peer->Receive())
	{
		auto packetID = GetPacketIdentifier(p);

		switch (packetID)
		{
#pragma region Base_Trans
			// Handle client packets
			case ID_CONNECTION_LOST:
			{
				printf("\nConnection lost from %s", p->systemAddress.ToString(true));
				cleanup();
				break;
			}
			case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			{ printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n"); break; }
			case ID_DISCONNECTION_NOTIFICATION:
			{ printf("ID_DISCONNECTION_NOTIFICATION\n"); break;}
			case ID_ALREADY_CONNECTED:
			{ printf("\nAlready connected with guid %s", p->guid); break; }
			case ID_CONNECTION_BANNED:
			{ printf("\nWe are banned from this server"); return false; }
			case ID_CONNECTION_ATTEMPT_FAILED:
			{ printf("\nConnection to server failed"); return false; }
			case ID_INVALID_PASSWORD:
			{ printf("\nPassword invalid"); return false; }
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Connection require ACCEPTED to %s with GUID %s\n",
					p->systemAddress.ToString(true), p->guid.ToString());
				printf("My external address is %s\n",
					a_peer->GetExternalID(p->systemAddress).ToString(true));

				unsigned char packet = (unsigned char)messages::ID_TTT_INIT_PLAYER;
				a_peer->Send((const char*)&(packet), sizeof(packet), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, false);
				break;
			}

			case messages::ID_TTT_OTHER_PLAYER_LOST_CONNECTION:
			{
				if (mPlayerID == 1)
				{
					printf("Player2 lost connection with the server.\n");
					mPlayerID = 1;
					printf("You are still Player %d. Waiting for Player 2.\n", mPlayerID);
				}
				else if (mPlayerID == 2)
				{
					printf("Player1 lost connection with the server.\n");
					mPlayerID = 1;
					printf("You are now Player %d. Waiting for Player 2.\n", mPlayerID);
				}

				cleanup();

				break;
			}

#pragma endregion

#pragma region Init_Game_Trans

			case messages::ID_TTT_PLAYER_NUM:
			{
				printf("Recieved player number");
				playerNumber *tmpPlayer = (playerNumber*)p->data;
				assert(p->length == sizeof(playerNumber));
				if (p->length != sizeof(playerNumber))
					return false;

				if (tmpPlayer->playerNum == 1)
				{
					//inits player data
					mPlayerID = 1;
					printf("\nWaiting for player 2.\n");
				}
				else if (tmpPlayer->playerNum == 2)
				{
					//inits player data
					mPlayerID = 2;
					printf("\nWaiting to start.\n");
				}
				break;
			}
			case messages::ID_TTT_GAME_START:
			{
				printf("Starting\n");

				mPlayer1Pad = new PhysicsObject;
				mPlayer2Pad = new PhysicsObject;//breaks here
				mBall = new PhysicsObject;
				//mpEvent = new sf::Event();

				mpRenderWindow = new RenderGame;
				mpRenderWindow->initWindow(mIsCommandLine);
				if (mpPhysics != nullptr)
				{
					//delete mpPhysics;
					mpPhysics = NULL;
					mpPhysics = new PhysicsObjectManager;
				}
				mpPhysics = mpRenderWindow->CreateBoard(mPlayer1Pad, mPlayer2Pad, mBall);
				CurrentTime = 0;
				break;
			}
#pragma endregion
#pragma region stateUpdate
			case messages::ID_TTT_PLAYER_UPDATE:
			{
				PlayerData* data = (PlayerData*)p->data;
				assert(p->length == sizeof(PlayerData));
				if (p->length != sizeof(PlayerData))
					return false;

				if (data->playerNum != mPlayerID)
				{
					if (mPlayerID == 1)
					{
						HardUpdate(mPlayer2Pad,data);
					}
					else
					{
						HardUpdate(mPlayer1Pad, data);
					}
				}

				break;
			}
			case messages::ID_TTT_STATE_BROADCAST:
			{
				//printf("\nRecieved Broadcast:");
				GameState* data = (GameState*)p->data;
				assert(p->length == sizeof(GameState));
				if (p->length != sizeof(GameState))
					return false;

				//score 
				mpRenderWindow->setP1Score(data->p1Score);
				mpRenderWindow->setP2Score(data->p2Score);

				if (data->Player1.playerNum == 1 && mPlayer1Pad != NULL)
				{
					//printf("\nPlayer1");
					HardUpdatePad(mPlayer1Pad, &(data->Player1));
				}
				if (data->Player2.playerNum == 2 && mPlayer2Pad != NULL)
				{
					//printf("\nPlayer2");
					HardUpdatePad(mPlayer2Pad, &(data->Player2));
				}
				if (mBall != NULL)
				{
					//printf("\nBall");
					HardUpdate(mBall, &(data->Ball));
				}
				
				//mpPhysics->resetBlocks();
				//for (int i = 0; i < data->blocks.size() - 1; i++)
				//{
				//	//mpPhysics->addPlaced(data->blocks[i]);
				//}
				bool test = true;
				int blockData;
				for (int i = 0; i < 9 && test; i++)
				{
					blockData = data->Blocks[i];
					if (blockData != -1)
					{
						mpPhysics->removeSpecificBlock(blockData);
					}
					else
					{
						test = false;
					}
				}
					//data->
					//HardUpdate()
				break;
			}
#pragma endregion

			case messages::ID_TTT_CHAT:
			{
				chatMessage *chat = (chatMessage*)p->data;
				printf("%s\n", chat->message);
				break;
			}

			default:
			{
				printf("\nReceived unhandled packet %c", GetPacketIdentifier(p));
				break;
			}
		}
	}
	return true;
}
void Client::Update(double deltaTime, RakNet::RakPeerInterface* a_peer)
{
	if (mpRenderWindow != nullptr)
	{
		PhysicsObject* targetMod;
		sf::Vector2i mousePos = mpRenderWindow->GetMousePosition();
		//printf("mouse y: %d\n", (int)mousePos.y);
		if (mousePos.y != mPriorMouse.y)
		{
			float yTo = (float)mousePos.y;
			float yFrom;
			float yNew;
			if (mPlayerID == 1)
			{
				targetMod = mPlayer1Pad;
			}
			else
			{
				targetMod = mPlayer2Pad;
			}

			yFrom = targetMod->GetPos().y;
			if (yTo < yFrom)
				yNew = -PAD_SPEED;
			else if (yTo > yFrom)
				yNew = PAD_SPEED;
			else
				yNew = 0;
			//yNew = (yTo < yFrom ? -PAD_SPEED : (yTo > yFrom ? PAD_SPEED : 0));
			//targetMod->SetVelocity(0, yNew);
			targetMod->SetFinalDest(targetMod->GetPos().x, mousePos.y);

			PlayerData ToSend;
			ToSend.CurPosX = targetMod->GetPos().x;
			ToSend.CurPosY = targetMod->GetPos().y;
			ToSend.xVel = 0;
			ToSend.yVel = 0;
			ToSend.DestPosY = mousePos.y;
			ToSend.DestPosX = targetMod->GetPos().x;
			ToSend.CurTime = CurrentTime;
			ToSend.playerNum = mPlayerID;
			ToSend.packetID = messages::ID_TTT_PLAYER_MOVED;

			a_peer->Send((const char*)&(ToSend), sizeof(PlayerData), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS , true);

		}
		mPriorMouse = mousePos;

		mpRenderWindow->Update(deltaTime);
		mpRenderWindow->Draw();
		CurrentTime += deltaTime;
	}
}
void Client::HardUpdate(PhysicsObject* object, PlayerData* data)
{
	object->hardUpdate(data->CurTime - CurrentTime, data->CurPosX, data->CurPosY, data->xVel, data->yVel,data->DestPosX,data->DestPosY);
};
void Client::HardUpdatePad(PhysicsObject* object, PlayerData* data)
{
	object->hardUpdate(0, data->CurPosX, data->CurPosY, data->xVel, data->yVel, data->DestPosX, data->DestPosY);
};