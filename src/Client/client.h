#pragma once
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "RakPeer.h"
#include "..\render\renderGame.h"
#include "..\render\CommonData.h"
#include <SFML\Window\Event.hpp>

class Client
{
	CONST float PAD_SPEED = 0.2f;
public:

	Client();
	~Client();
	unsigned char GetPacketIdentifier(RakNet::Packet *p);
	bool LaunchClient(RakNet::RakPeerInterface* a_peer, int a_clientPort, const char* a_serverIP, int a_serverPort, bool isCommandLine);
	bool HandlePackets(RakNet::RakPeerInterface* a_peer);

	void Update(double deltaTime, RakNet::RakPeerInterface* a_peer);

	inline RenderGame* getRenderWindow(){ return mpRenderWindow; };

private:
	void cleanup();
	void HardUpdate(PhysicsObject* object, PlayerData* data);
	void HardUpdatePad(PhysicsObject* object, PlayerData* data);
	double CurrentTime;
	sf::Vector2i mPriorMouse;

	PhysicsObjectManager* mpPhysics;

	RenderGame* mpRenderWindow;
	PhysicsObject* mPlayer1Pad;
	PhysicsObject* mPlayer2Pad;
	PhysicsObject* mBall;

	//players mPlayer
	chatMessage mChat;
	broadcastMessage mbroadcast;
	playerMovement mMovement;

	sf::Event* mpEvent;
	int mPlayerID;
	bool mIsCommandLine = false;
};