#pragma once
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "RakPeer.h"

//class PhysicsObject;

namespace messages {
	enum value_type : unsigned char
	{
		ID_TTT_TURN_AND_BOARD = ID_USER_PACKET_ENUM,
		ID_TTT_OTHER_USER_WON,
		ID_TTT_INIT_PLAYER,//give me my number!
		ID_TTT_PLAYER_NUM,//receiving number!
		ID_TTT_GAME_START,//all players registered.
		ID_TTT_PLAYER_UPDATE,//update a single player pos.(hard update)
		ID_TTT_STATE_BROADCAST,//update the game state.
		ID_TTT_PLAYER_MOVED,//send the movement to the server
		ID_TTT_CHAT,//just a chat message
		ID_TTT_OTHER_PLAYER_LOST_CONNECTION
	};
};
/*
	A NOTE ON DATA:
	The destination position is a limiting factor on the objects velocity, not where the object should be.
	For the players, this is where there mouse is located, to stop the paddles from exeding a certain place
	For the Ball, this is a nonsensical value, impossible to reach.
*/
//player data.
#pragma pack(push, 1)
struct PlayerData
{
	unsigned char packetID;
	int playerNum = 0;
	float xVel;
	float yVel;
	float CurPosX;
	float CurPosY;
	float DestPosX;
	float DestPosY;
	double CurTime;
};
#pragma pop
//game state to force players.
#pragma pack(push, 1)
struct GameState
{
	unsigned char packetID;
	PlayerData Player1;
	PlayerData Player2;
	PlayerData Ball;

	int p1Score;
	int p2Score;

	//std::vector<PhysicsObject*> blocks;
	int Blocks[10];
	//need to add data for field here. if there are problems.
};
#pragma pop
#pragma pack(push, 1)
struct GameStart
{
	unsigned char packetID;
	double CurrentTime;
};
#pragma pop
#pragma pack(push, 1)
struct playerNumber
{
	unsigned char packetID;
	int playerNum = 0;
};
#pragma pop
#pragma pack(push, 1)
struct player
{
	unsigned char packetID;
	int playerNum = 0;
	int x, y;
};
#pragma pop
#pragma pack(push, 1)
struct chatMessage
{
	unsigned char packetID;
	char message[2028];
};
#pragma pop
#pragma pop
struct playerMovement
{
	unsigned char packetID;
	int playerNum;
	int keyPressed;
};
#pragma pack(push, 1)
#pragma pack(push, 1)
struct broadcastMessage
{
	unsigned char packetID;
	int player1x;
	int player1y;

	int player2x;
	int player2y;

	int ballx;
	int bally;
};
#pragma pop

const int RAKNET_UPDATE_VALUE = 30;
const int WINDOW_HEIGHT = 300;
const int WINDOW_WIDTH = 610;

//how many units do the boxes extind out? 3= 3 layers of boxes.
static const int BOX_DEPTH = 3;
//how high, in board units, is the paddle?
static const int PADDLE_HEIGHT = 3;
//how far out is the paddle from the boxes?
static const int PADDLE_PAD = 1;
static const int SIZE_SCALE = 20;
//how many units is the board wide and high?
static const int BOARD_WIDTH = 30;
static const int BOARD_HEIGHT = 10;
static const int GAME_HEIGHT = BOARD_HEIGHT*SIZE_SCALE;
static const int GAME_WIDTH = BOARD_WIDTH*SIZE_SCALE;