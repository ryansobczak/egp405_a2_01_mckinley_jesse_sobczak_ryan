#pragma once
//Force commit by adding comment.
#include "PhysicsObject.h"
#include "..\render\CommonData.h"
//include map

const float maxBoarder = (float)WINDOW_HEIGHT - 25.0f;
const float minBoarder = 25.0f;

typedef std::map<int, PhysicsObject*> ObjectList;
typedef std::pair<int, PhysicsObject*> ObjectPair;

class PhysicsObjectManager
{
public:
	PhysicsObjectManager();
	~PhysicsObjectManager();

	void addMoving(PhysicsObject*);
	void Reset(){ index = 0; };
	void addControlled(PhysicsObject*);
	void addPlaced(PhysicsObject*);

	void update(double deltaTime);

	void draw(sf::RenderWindow* window, sf::Text player1, sf::Text player2);

	bool doesBlockExist(int ind);
	PhysicsObject* getSpecificBlock(int ind);
	void removeSpecificBlock(int index);
	void resetBlocks();

	inline int getBlocksNum(){ return Placed.size(); };
	inline int getP1Score(){ return p1Score; };
	inline int getP2Score(){ return p2Score; };
private:
	
	int index;

	ObjectList Moving;
	ObjectList Controlled;
	ObjectList Placed;
	int mIndexStart;
	
	float upperBound;
	float lowerBound;

	int p1Score;
	int p2Score;
};