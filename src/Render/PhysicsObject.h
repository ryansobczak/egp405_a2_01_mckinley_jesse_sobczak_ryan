#pragma once

//Force commit by adding comment.
#include <SFML\Graphics.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Window.hpp>
#include "..\render\CommonData.h"


class CollisionData
{
public:
	typedef char ColType;
	CollisionData(sf::Shape* object){ BaseObject = object; };
	virtual bool GetCollision(sf::Shape* obj) = 0;
protected:
	sf::Shape* BaseObject;
};
class CircleCollider : public CollisionData
{
public:
	CircleCollider(float radius, sf::Shape* object);
	bool GetCollision(sf::Shape* obj);
protected:
	float mRadius;
};
class RectCollider : public CollisionData
{
public:
	RectCollider(sf::Shape* object);
	bool GetCollision(sf::Shape* obj);
};

class PhysicsObject
{
public:
	const int RAND_VARIANCE = 5;
	const float RAND_MULT = 0.1f;
	PhysicsObject();
	~PhysicsObject();
	

	void SetCircle(float radius);
	void SetRect( float w, float h, bool defaultOrigin = true);
	void SetColor(sf::Color color);

	void SetCheckDest(bool value) { CheckDest = value; }
	void SetVelocity(float x,float y);
	void SetFinalDest(float x, float y);
	void SetPos(float x, float y);
	sf::Vector2f GetPos() { return CurPos; };
	inline sf::Vector2f getDestPos(){ return DestPos; };
	inline float getXVel(){ return xVel; };
	inline float getYVel(){ return yVel; };

	void update(double deltaTime);
	void hardUpdate(float deltaTime,float pX, float pY, float vX, float vY, float dX, float dY);

	bool CheckCollision(PhysicsObject* obj);

	void BorderCollision( float yMin, float yMax );
	
	void draw(sf::RenderWindow* window);
	inline bool getBallLeft(){ return ballLeftPoint; };
	inline bool getBallRight(){ return ballRightPoint; };

	inline void resetBallLeft(){ ballLeftPoint = false; };
	inline void resetBallRight(){ ballRightPoint = false; };

	int Lerp(int a, int b, double a_time);
	float getRadius(sf::Vector2f startPoint, sf::Vector2f endPoint);
private:

	CollisionData::ColType SavedCollision;
	
	bool CheckDest;
	bool SavedData;

	CollisionData* collider;
	sf::Shape* mShape;
	
	float xVel = 0;
	float yVel = 0;
	sf::Vector2f CurPos;
	sf::Vector2f PrevPos;
	sf::Vector2f DestPos;
	float PrevDelta;
	
	sf::Vector2f SavedPos;
	float SavedTime;

	bool ballLeftPoint = false;
	bool ballRightPoint = false;

	float mTime = 0;
	const float TIME_STEP = 0.4f;
	const int MAX_TIME = 1;
	const int DEAD_REC_CONTROLLED = 400;
	const int DEAD_REC_MOVING = 500;
};