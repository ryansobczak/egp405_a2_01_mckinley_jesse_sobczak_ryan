#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window.hpp>
#include <vector>
#include "PhysicsObjectManager.h"

class RenderGame 
{
public:
	RenderGame();
	~RenderGame();
	void initWindow(bool isCommandLine);
	sf::CircleShape addShape(int red, int blue, int green);
	sf::Vector2f update(sf::CircleShape circle, int x, int y);
	void draw(sf::CircleShape circle);
	void clearScreen();
	void displayScreen();

	sf::Vector2i GetMousePosition();

	void Update(double deltaT);
	void Draw();
	//Pass the pointer to pointers used by the client, generate the object in here. pad 1 is player 1.
	PhysicsObjectManager* CreateBoard(PhysicsObject* Pad1, PhysicsObject* Pad2, PhysicsObject* Ball);
	static void CreateBoard(PhysicsObject* Pad1, PhysicsObject* Pad2, PhysicsObject* Ball, PhysicsObjectManager* Physics);

	inline sf::CircleShape& getCircle(){ return mCircle; };
	inline sf::RenderWindow& getWindow(){ return mWindow; };

	//inline int getHeight(){ return WINDOW_HEIGHT; };
	//inline int getWidth(){ return WINDOW_WIDTH; };
	inline int getP1Score(){ return mP1Score; };
	inline int getP2Score(){ return mP2Score; };

	inline void setP1Score(int score){ mP1Score = score; };
	inline void setP2Score(int score){ mP2Score = score; };
private:
	//static const int WINDOW_HEIGHT = 300;
	//static const int WINDOW_WIDTH = 610;
	const std::string WINDOW_NAME = "Game Window";
	const std::string COMMAND_WINDOW_FONT = "framdit.ttf";
	const std::string NON_COMMAND_LINE_FONT = "../src/Assets/framdit.ttf";

	bool commandLine = false;

	sf::CircleShape mCircle;
	sf::RenderWindow mWindow;
	sf::Event mEvent;

	PhysicsObjectManager Manager;

	sf::Font mpFont;
	sf::Text mPlayer1score;
	sf::Text mPlayer2score;
	int mP1Score;
	int mP2Score;
};