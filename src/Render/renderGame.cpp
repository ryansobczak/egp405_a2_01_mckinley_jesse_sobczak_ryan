#include "renderGame.h"
#include "SFML\System\Vector2.hpp"
#include "..\render\CommonData.h"

#include<iostream>
#include<SFML/System/String.hpp>

RenderGame::RenderGame(){}

RenderGame::~RenderGame() {}

void RenderGame::initWindow(bool isCommandLine)
{
	commandLine = isCommandLine;
	//mWindow = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_NAME);

	//might break cuz the fonts

	//sf::RenderWindow* mpWin = new sf::RenderWindow(sf::VideoMode(800, 800), "Game");
	mWindow.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_NAME);
	//mWindow->setFramerateLimit(60);
	
	if (!commandLine)
		mpFont.loadFromFile(NON_COMMAND_LINE_FONT);
	if (commandLine)
		mpFont.loadFromFile(COMMAND_WINDOW_FONT);

	mPlayer1score.setFont(mpFont); 
	mPlayer1score.setString("Player 1 score: 0");
	mPlayer1score.setCharacterSize(18); 
	mPlayer1score.setPosition(150, 10);
	mPlayer1score.setColor(sf::Color::Black);

	mPlayer2score.setFont(mpFont);
	mPlayer2score.setString("Player 2 score: 0");
	mPlayer2score.setCharacterSize(18);
	mPlayer2score.setPosition(350, 265);
	mPlayer2score.setColor(sf::Color::Black);
}

sf::CircleShape RenderGame::addShape(int red, int blue, int green)
{
	sf::CircleShape circle(50);
	circle.setOrigin(sf::Vector2f(50, 50));
	circle.setFillColor(sf::Color(red, blue, green));

	mCircle = circle;
	return mCircle;
}
//int Lerp(int a, int b, double a_time)
//{
//	return (1 - a_time)* a + a_time * b;
//}
//ClientState Lerp(ClientState a, ClientState b, double a_time)
//{
//	ClientState ls;
//	ls.x = Lerp(a.x, b.x, a_time);
//	ls.y = Lerp(a.y, b.y, a_time);
//}
//
//void RenderGame::update(double a_deltaT)
//{
//	updateRakNet();

//	getUserState();
//
//	ClientState cs;
//	cs.x = 10;
//	cs.y = 11;
//
//	ClientState csFromServer;
//	csFromServer.x = 11;
//	csFromServer.y = 15;
//
//
//	//cap delta time to 30
//
//	/*for (unsigned int i = 1; i <= mCircleVector.size(); i++)
//	{
//		mCircleVector[i - 1].setPosition(static_cast<sf::Vector2f>(pos));
//	}*/
//}
sf::Vector2f RenderGame::update(sf::CircleShape circle, int x, int y)
{
	sf::Vector2f vec;
	vec.x = (float)x;
	vec.y = (float)y;
	return vec;
}

void RenderGame::draw(sf::CircleShape circle)
{
	mWindow.draw(circle);
}

void RenderGame::clearScreen()
{
	mWindow.clear(sf::Color::White);
}

void RenderGame::displayScreen()
{
	mWindow.display();
}

void RenderGame::Update(double deltaT)
{
	sf::Event eEvent;
	while (mWindow.pollEvent(eEvent))
	{

	}
	Manager.update(deltaT);

	//updates score
	mPlayer1score.setString("Player 1 score: " + std::to_string(Manager.getP1Score()));
	mPlayer2score.setString("Player 2 score: " + std::to_string(Manager.getP2Score()));
};
void RenderGame::Draw()
{
	Manager.draw(&mWindow, mPlayer1score, mPlayer2score);
};
PhysicsObjectManager* RenderGame::CreateBoard(PhysicsObject* Pad1, PhysicsObject* Pad2, PhysicsObject* Ball)
{
	CreateBoard(Pad1, Pad2, Ball, &Manager);
	return &Manager;
};
void RenderGame::CreateBoard(PhysicsObject* Pad1, PhysicsObject* Pad2, PhysicsObject* Ball, PhysicsObjectManager* Physics)
{
	Ball->SetCircle(SIZE_SCALE / 4);
	Ball->SetColor(sf::Color::Blue);
	Ball->SetPos(GAME_WIDTH / 2, GAME_HEIGHT / 2);
	Ball->SetVelocity(0.1f,0);
	Physics->addMoving(Ball);
	Physics->Reset();

	Pad1->SetRect((float)SIZE_SCALE, (float)SIZE_SCALE*PADDLE_HEIGHT, false);
	Pad1->SetPos((BOX_DEPTH + PADDLE_PAD)*SIZE_SCALE, GAME_HEIGHT / 2);
	Pad1->SetVelocity(0, 0);
	Pad1->SetColor(sf::Color::Red);
	Physics->addControlled(Pad1);

	Pad2->SetRect((float)SIZE_SCALE, (float)SIZE_SCALE*PADDLE_HEIGHT, false);
	Pad2->SetPos(GAME_WIDTH - ((BOX_DEPTH + PADDLE_PAD)*SIZE_SCALE), GAME_HEIGHT / 2);
	Pad2->SetVelocity(0,0);
	Pad2->SetColor(sf::Color::Red);
	Physics->addControlled(Pad2);
	Physics->Reset();

	PhysicsObject* box;

	//right side
	for (int i = 0; i < WINDOW_HEIGHT/SIZE_SCALE; i++)
	{
		for (int j = 0; j < BOX_DEPTH - 1; j++)
		{
			box = new PhysicsObject();
			box->SetRect((float)SIZE_SCALE, (float)SIZE_SCALE);
			box->SetColor(sf::Color::Cyan);
			box->SetPos(j*(float)SIZE_SCALE, i*(float)SIZE_SCALE);
			Physics->addPlaced(box);
		}
	}
	//left side
	for (int a = 0; a < WINDOW_HEIGHT / SIZE_SCALE; a++)
	{
		for (int b = 1; b < BOX_DEPTH; b++)
		{
			box = new PhysicsObject();
			box->SetRect((float)SIZE_SCALE, (float)SIZE_SCALE);
			box->SetColor(sf::Color::Cyan);
			box->SetPos(((float)WINDOW_WIDTH - b * (float)SIZE_SCALE), a*(float)SIZE_SCALE);
			Physics->addPlaced(box);
		}
	}
};

sf::Vector2i RenderGame::GetMousePosition()
{
	return sf::Mouse::getPosition(mWindow);
}