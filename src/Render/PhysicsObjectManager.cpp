#include "PhysicsObjectManager.h"

//Force commit by adding comment.
PhysicsObjectManager::PhysicsObjectManager()
{
	index = 0;
	p1Score = 0;
	p2Score = 0;
};
PhysicsObjectManager::~PhysicsObjectManager()
{
	for each (ObjectPair pair in Moving)
	{
		delete pair.second;
	}
	for each (ObjectPair pair in Controlled)
	{
		delete pair.second;
	}
	for each (ObjectPair pair in Placed)
	{
		delete pair.second;
	}
	Placed.clear();
	Moving.clear();
	Controlled.clear();
};
void PhysicsObjectManager::addMoving(PhysicsObject* obj)
{
	ObjectPair pair(index++, obj);
	Moving.insert(pair);
};
void PhysicsObjectManager::addControlled(PhysicsObject* obj)
{
	ObjectPair pair(index++, obj);
	Controlled.insert(pair);
};
void PhysicsObjectManager::addPlaced(PhysicsObject* obj)
{
	ObjectPair pair(index++, obj);
	Placed.insert(pair);
};

void PhysicsObjectManager::update(double deltaTime)
{
	PhysicsObject* target;
	for each (ObjectPair pair in Controlled)
	{
		target = pair.second;
		target->update(deltaTime);
		target->BorderCollision(minBoarder, maxBoarder);
	}
	for each (ObjectPair pair in Moving)
	{
		target = pair.second;
		target->update(deltaTime);

		if (target->getBallLeft() == true)
		{
			p2Score++;
			target->resetBallLeft();
		}
		else if (target->getBallRight() == true)
		{
			p1Score++;
			target->resetBallRight();
		}

		for (auto test = Placed.begin(); test != Placed.end();)
		{
			if ((target->CheckCollision)(test->second))
			{
				delete test->second;
				Placed.erase(test++);
			}
			else
				test++;
		}
		for each(ObjectPair test in Controlled)
		{
			(target->CheckCollision)(test.second);
		}
		target->BorderCollision(minBoarder, maxBoarder);
	}
};
void PhysicsObjectManager::draw(sf::RenderWindow* window, sf::Text player1, sf::Text player2)
{
	window->clear(sf::Color(125, 125, 125));

	window->draw(player1);
	window->draw(player2);

	for each (ObjectPair pair in Moving)
	{
		pair.second->draw(window);
	}
	for each (ObjectPair pair in Controlled)
	{
		pair.second->draw(window);
	}
	for each (ObjectPair pair in Placed)
	{
		pair.second->draw(window);
	}

	window->display();
};


bool PhysicsObjectManager::doesBlockExist(int ind)
{
	return (Placed.find(ind) != Placed.end());
};
PhysicsObject* PhysicsObjectManager::getSpecificBlock(int index)
{ 
	PhysicsObject* target;
	for each (ObjectPair pair in Placed)
	{
		if (pair.first == index)
		{
			target = pair.second;
			return target;
		}
	}

	return NULL; 
}
void PhysicsObjectManager::removeSpecificBlock(int index)
{

};

void PhysicsObjectManager::resetBlocks()
{
	for each (ObjectPair pair in Moving)
	{
		delete pair.second;
	}
	Moving.clear();
}