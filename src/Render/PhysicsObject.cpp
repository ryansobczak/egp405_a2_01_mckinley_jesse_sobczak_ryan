#include "PhysicsObject.h"

#include <random>
#include <math.h>
using namespace std;
//Force commit by adding comment.
PhysicsObject::PhysicsObject()
{
};
PhysicsObject::~PhysicsObject(){};

void PhysicsObject::SetCircle(float radius)
{
	mShape = new sf::CircleShape(radius);
	collider = new CircleCollider(radius, mShape);
	mShape->setOrigin(radius,radius);
	CheckDest = false;
};
void PhysicsObject::SetRect(float w, float h, bool defaultOrigin)
{
	sf::Vector2f temp(w, h);
	mShape = new sf::RectangleShape(temp);
	collider = new RectCollider(mShape);
	if (!defaultOrigin) mShape->setOrigin(w / 2, h / 2);
	CheckDest = true;
}
void PhysicsObject::SetColor(sf::Color color)
{
	mShape->setFillColor(color);
};
	
void PhysicsObject::SetVelocity(float x,float y)
{
	xVel = x;
	yVel = y;
};
void PhysicsObject::SetFinalDest(float x, float y)
{
	DestPos.x = x;
	DestPos.y = y;
};
void PhysicsObject::update(double deltaTime)
{
	PrevDelta = (float)deltaTime;
	PrevPos = CurPos = mShape->getPosition();

	if (xVel != 0)
		CurPos.x += xVel*(float)deltaTime;
	if (yVel != 0)
		CurPos.y += yVel*(float)deltaTime;

	//resets ball
	if (CurPos.x > WINDOW_WIDTH + 25)
	{
		//Player 1 scores
		ballRightPoint = true;
		CurPos.x = WINDOW_WIDTH / 2;
	}
	else if (CurPos.x < -25)
	{
		//player 2 scores
		ballLeftPoint = true;
		CurPos.x = WINDOW_WIDTH / 2;
	}

	if (CheckDest)
	{
		if (mTime <= MAX_TIME)
			mTime += TIME_STEP;
		else
			mTime = 0;

		CurPos.y = CurPos.y + mTime*(DestPos.y - CurPos.y);
		CurPos.x = CurPos.x + mTime*(DestPos.x - CurPos.x);

		//CurPos.x = Lerp(CurPos.x, DestPos.x, deltaTime);
		//CurPos.y = Lerp(CurPos.y, DestPos.y, deltaTime);
		/*if (xVel > 0) 
		{
			if (yVel > 0)
			{
				if (CurPos.x >= DestPos.x && CurPos.y >= DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
			else if (yVel < 0)
			{
				if (CurPos.x >= DestPos.x && CurPos.y <= DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
			else
			{
				if (CurPos.x >= DestPos.x && CurPos.y == DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
		}
		else if (xVel < 0)
		{
			if (yVel > 0)
			{
				if (CurPos.x <= DestPos.x && CurPos.y >= DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
			else if (yVel < 0)
			{
				if (CurPos.x <= DestPos.x && CurPos.y <= DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
			else
			{
				if (CurPos.x <= DestPos.x && CurPos.y == DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
		}
		else
		{
			if (yVel > 0)
			{
				if (CurPos.x == DestPos.x && CurPos.y >= DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
			else if (yVel < 0)
			{
				if (CurPos.x == DestPos.x && CurPos.y <= DestPos.y)
				{
					CurPos = DestPos;
					yVel = 0;
					xVel = 0;
				}
			}
		}*/

	}


	mShape->setPosition(CurPos);
};
void PhysicsObject::SetPos(float x, float y)
{
	CurPos.x = x;
	CurPos.y = y;
	mShape->setPosition(CurPos);
};
	
void PhysicsObject::hardUpdate(float deltaTime,float pX, float pY, float vX, float vY, float dX, float dY)
{
	//CurPos.x = pX;// -deltaTime*vX;
	//CurPos.y = pY;// -deltaTime*vY;

	xVel = vX;
	yVel = vY;
	DestPos.x = dX;
	DestPos.y = dY;

	//paddles
	if (getRadius(CurPos, sf::Vector2f(pX, pY)) >= DEAD_REC_CONTROLLED && xVel == 0 && yVel == 0)
	{
		CurPos.x = pX;// -deltaTime*vX;
		CurPos.y = pY;// -deltaTime*vY;
		mShape->setPosition(CurPos);
	}
	else if ((getRadius(CurPos, sf::Vector2f(pX, pY)) >= DEAD_REC_MOVING && (xVel != 0 || yVel != 0)))
	{
		CurPos.x = pX;// -deltaTime*vX;
		CurPos.y = pY;// -deltaTime*vY;
		mShape->setPosition(CurPos);
	}

};
void PhysicsObject::BorderCollision(float yMin, float yMax)
{
	//float correction;
	if(CurPos.y < yMin)
	{
		//correction = yMin - CurPos.y;
		CurPos.y = yMin;// +correction;
		yVel = -yVel;
		mShape->setPosition(CurPos);
	}
	else if(CurPos.y > yMax)
	{
		//correction = CurPos.y - yMax;
		CurPos.y = yMax;// -correction;
		yVel = -yVel;
		mShape->setPosition(CurPos);
	}
};
void PhysicsObject::draw(sf::RenderWindow* window)
{
	window->draw(*mShape);
};

bool PhysicsObject::CheckCollision(PhysicsObject* obj)
{ 
	bool collision = collider->GetCollision(obj->mShape);

	if (collision)
	{

			float vel = sqrt(xVel*xVel + yVel*yVel);

			float relativeY = CurPos.y - obj->CurPos.y;

			float normalizeY = relativeY / (obj->mShape->getLocalBounds().height / 2);
			float bounceAngle = normalizeY * (5 * std::_Pi / 12);

			xVel = vel*cos(bounceAngle);
			yVel = vel*sin(bounceAngle);
			//ballVx = BALLSPEED*Math.cos(bounceAngle);
			//ballVy = BALLSPEED*-Math.sin(bounceAngle);
			if (CurPos.x < obj->CurPos.x)
			{
				xVel = -xVel;
			}
	}

	return collision;
};

CircleCollider::CircleCollider(float radius, sf::Shape* object) : CollisionData(object)
{
	mRadius = radius;
};
bool CircleCollider::GetCollision(sf::Shape* obj)
{
	sf::FloatRect tempCheck = obj->getGlobalBounds();
	sf::FloatRect base = BaseObject->getGlobalBounds();
	return (tempCheck.intersects(base));
};
RectCollider::RectCollider(sf::Shape* object) : CollisionData(object)
{
};
bool RectCollider::GetCollision(sf::Shape* obj)
{
	sf::FloatRect tempCheck = obj->getGlobalBounds();
	sf::FloatRect base = BaseObject->getGlobalBounds();
	return (tempCheck.intersects(base));
};

int PhysicsObject::Lerp(int a, int b, double a_time)
{
	return (1 - a_time)* a + a_time * b;
}

float PhysicsObject::getRadius(sf::Vector2f startPoint, sf::Vector2f endPoint)
{

	float newX = endPoint.x - startPoint.x;

	float newY = endPoint.y - startPoint.y;

	return (newX*newX) + (newY*newY);

}
