#include "server.h"
#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "Gets.h"
#include <fstream>

Server::Server()
{
	mPlayer1Pad = new PhysicsObject;
	mPlayer2Pad = new PhysicsObject;
	mBall = new PhysicsObject;
	mpPhysics = new PhysicsObjectManager;
	mCurrentTime = 0;
}

Server::~Server()
{
	delete mpPhysics;
}


unsigned char Server::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

bool Server::LaunchServer(RakNet::RakPeerInterface* a_server, int a_port, bool isCommandLine)
{
	mIsCommandLine = isCommandLine;

	mCurrentTime = 0;
	std::ofstream file("IPAddress.txt");
	a_server->SetIncomingPassword(0, 0);

	// IPV4 socket
	RakNet::SocketDescriptor  sd;
	sd.port = a_port;
	sd.socketFamily = AF_INET;
	printf("Server port: %d\n", a_port);

	if (a_server->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
	{
		printf("\nFailed to start server with IPV4 ports");
		return false;
	}

	a_server->SetOccasionalPing(true);
	a_server->SetUnreliableTimeout(1000);
	a_server->SetTimeoutTime(4000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	a_server->SetMaximumIncomingConnections(4);

	printf("\nSERVER IP addresses:");
	for (unsigned int i = 0; i < a_server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = a_server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("\n%i. %s (LAN=%i)", i + 1, sa.ToString(false), sa.IsLANAddress());
	}
	file << a_server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, 0).ToString();
	file.close();
	return true;
}

bool Server::HandlePackets(RakNet::RakPeerInterface* a_peer)
{
#pragma region ToEdit
	mCounter++;
	if ((mCounter > RAKNET_UPDATE_VALUE) && (mStartedGame))
	{
		mGameState.p1Score = mpRenderWindow->getP1Score();
		mGameState.p2Score = mpRenderWindow->getP2Score();

		//printf("\Sending Broadcast\n");
		//ball states
		mGameState.Ball.CurPosX = mBall->GetPos().x;
		mGameState.Ball.CurPosY = mBall->GetPos().y;
		mGameState.Ball.DestPosX = mBall->getDestPos().x;
		mGameState.Ball.DestPosY = mBall->getDestPos().y;
		mGameState.Ball.xVel = mBall->getXVel();
		mGameState.Ball.yVel = mBall->getYVel();
		mGameState.Ball.CurTime = mCurrentTime;
		int index = 0;
		int num = PriorStateBlocks.size();
		for (unsigned int i = 0; i < num; i++)
		{
			if (!mpPhysics->doesBlockExist(PriorStateBlocks[i]))
			{
				mGameState.Blocks[index++] = PriorStateBlocks[i];
				PriorStateBlocks.erase(i+PriorStateBlocks.begin());
				i--;
				num--;
			}
		}
		mGameState.Blocks[index] = -1;
		/*for (int i = mGameState.blocks.size(); i < 0; i--)
		{
			delete mGameState.blocks[i - 1];
		}
		mGameState.blocks.clear();
		for (int j = 0; j < mpPhysics->getBlocksNum() - 1; j++)
		{
			mGameState.blocks.push_back(mpPhysics->getSpecificBlock(j));
		}*/

		//printf("block num: %d\n", mpPhysics->getBlocksNum());
		//printf("game state num: %d\n", mGameState.blocks.size());

		mGameState.Player1.CurPosX = (BOX_DEPTH + PADDLE_PAD)*SIZE_SCALE;
		mGameState.Player1.CurPosY = mPlayer1Pad->GetPos().y;
		mGameState.Player1.DestPosX = (BOX_DEPTH + PADDLE_PAD)*SIZE_SCALE;
		mGameState.Player1.DestPosY = mPlayer1Pad->getDestPos().y;
		mGameState.Player1.xVel = 0;
		mGameState.Player1.yVel = 0;
		mGameState.Player1.CurTime = mCurrentTime;
		mGameState.Player1.playerNum = 1;


		mGameState.Player2.CurPosX = GAME_WIDTH - ((BOX_DEPTH + PADDLE_PAD)*SIZE_SCALE);
		mGameState.Player2.CurPosY = mPlayer2Pad->GetPos().y;
		mGameState.Player2.DestPosX = GAME_WIDTH - ((BOX_DEPTH + PADDLE_PAD)*SIZE_SCALE);
		mGameState.Player2.DestPosY = mPlayer2Pad->getDestPos().y;
		mGameState.Player2.xVel = 0;
		mGameState.Player2.yVel = 0;
		mGameState.Player2.CurTime = mCurrentTime;
		mGameState.Player2.playerNum = 2;

		mGameState.packetID = (unsigned char)messages::ID_TTT_STATE_BROADCAST;
		a_peer->Send((const char*)&mGameState, sizeof(mGameState), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		mCounter = 0;
	}
#pragma endregion
	for (RakNet::Packet* p = a_peer->Receive(); p; a_peer->DeallocatePacket(p), p = a_peer->Receive())
	{
		auto packetID = GetPacketIdentifier(p);

		switch (packetID)
		{
#pragma region Base_Trans
			case ID_DISCONNECTION_NOTIFICATION:
			{
				printf("ID_DISCONNECTION_NOTIFICATION from %s\n", p->systemAddress.ToString(true));;
				break;
			}
			case ID_CONNECTION_LOST:
			{ 
				printf("\nConnection lost from %s", p->systemAddress.ToString(true));

				currentPlayers--;
				if (currentPlayers < 0) 
					currentPlayers = 0;

				mStartedGame = false;
				if (mpPhysics != nullptr)
				{
					//delete mpPhysics;
					mpPhysics = nullptr;
				}
				unsigned char packetID = (unsigned char)messages::ID_TTT_OTHER_PLAYER_LOST_CONNECTION;
				a_peer->Send((const char*)&(packetID), sizeof(packetID), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
				break; 
			}
			case ID_NEW_INCOMING_CONNECTION:
			{
				printf("\nNew incoming connection from %s with GUID %s",
					p->systemAddress.ToString(true), p->guid.ToString());
				break;
			}
#pragma endregion
#pragma region Init_Game_Trans
			case messages::ID_TTT_INIT_PLAYER:
			{
				printf("Request from %s for player number", p->systemAddress.ToString(true));
				if (currentPlayers < 2)
				{
					playerNumber newPlayer;
					newPlayer.packetID = messages::ID_TTT_PLAYER_NUM;
					newPlayer.playerNum = ++currentPlayers;
					a_peer->Send((const char*)&(newPlayer), sizeof(playerNumber), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, false);

					if (currentPlayers >= 2)
					{

						mpRenderWindow = new RenderGame;
						if (mpPhysics == nullptr)
						{
							//delete mpPhysics;
							mpPhysics = new PhysicsObjectManager;
						}
						//mpRenderWindow->initWindow(mIsCommandLine);
						mpPhysics = mpRenderWindow->CreateBoard(mPlayer1Pad, mPlayer2Pad, mBall);
						//RenderGame::CreateBoard(mPlayer1Pad, mPlayer2Pad, mBall, mpPhysics);

						int num = mpPhysics->getBlocksNum();
						PriorStateBlocks.resize(num);
						for (int i = 0; i < num; i++)
						{
							PriorStateBlocks[i] = i;
						}
						
						mStartedGame = true;
						mCurrentTime = 0;
						unsigned char packetID = messages::ID_TTT_GAME_START; 
						a_peer->Send((const char*)&(packetID), sizeof(packetID), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
					}
				}
				break;
			}
#pragma endregion
#pragma region GameUpdate
			case messages::ID_TTT_PLAYER_MOVED:
			{
				PlayerData* data = (PlayerData*)p->data;
				assert(p->length == sizeof(PlayerData));
				if (p->length != sizeof(PlayerData))
					return false;

				if (data->playerNum == 1)
				{
					mGameState.Player1 = *data;
					mPlayer1Pad->hardUpdate(data->CurTime - mCurrentTime, data->CurPosX, data->CurPosY, data->xVel, data->yVel, data->DestPosX, data->DestPosY);
					//broadcast to all but the originator.
					data->CurPosX = mPlayer1Pad->GetPos().x;
					data->CurPosY = mPlayer1Pad->GetPos().y;
					data->CurTime = mCurrentTime;
				}
				else if (data->playerNum == 2)
				{
					mGameState.Player2 = *data;
					mPlayer2Pad->hardUpdate(data->CurTime - mCurrentTime, data->CurPosX, data->CurPosY, data->xVel, data->yVel, data->DestPosX, data->DestPosY);
					//broadcast to all but the originator.
					data->CurPosX = mPlayer2Pad->GetPos().x;
					data->CurPosY = mPlayer2Pad->GetPos().y;
					data->CurTime = mCurrentTime;
				}
				data->packetID = messages::ID_TTT_PLAYER_UPDATE;
				a_peer->Send((const char*)data, sizeof(PlayerData), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, true);

				break;
			}
			case messages::ID_TTT_PLAYER_UPDATE:
			{
				break;
			}
#pragma endregion
			case messages::ID_TTT_CHAT:
			{
				chatMessage *chat = (chatMessage*)p->data;
				mChat = *chat;
				//printf("%s\n", chat->message);
				mChat.packetID = (unsigned char)messages::ID_TTT_CHAT;
				a_peer->Send((const char*)&(mChat), sizeof(mChat), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, true);
				break;
			}

			default:
			{
				printf("%s\n", p->data);
				break;
			}
		}
	}

	return true;
}
void Server::Update(double deltaTime)
{
	if (currentPlayers >= 2)
	{
		mpRenderWindow->Update(deltaTime);
		mpRenderWindow->Draw();
		mCurrentTime += deltaTime;
	}
};