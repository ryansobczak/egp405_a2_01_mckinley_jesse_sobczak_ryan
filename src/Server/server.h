#pragma once
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "RakPeer.h"
#include "..\render\renderGame.h"

#include "..\render\CommonData.h"
//struct ClientState
//{
//	long unsigned int m_id;
//	int x, y;
//};
//struct SeverState
//{
//
//};
//struct players
//{
//	unsigned char packetID;
//	//player player1;
//	//player player2;
//};
//#pragma pop


class Server
{
public:
	Server();
	~Server();
	int main();
	unsigned char GetPacketIdentifier(RakNet::Packet *p);
	bool LaunchServer(RakNet::RakPeerInterface* a_peer, int a_port, bool isCommandLine);
	bool HandlePackets(RakNet::RakPeerInterface* a_peer);

	//implement later
	void Update(double deltaTime);

private:
	std::vector<int> PriorStateBlocks;
	//may not need, may replace with physics object manager.
	PhysicsObjectManager* mpPhysics;

	PhysicsObject* mPlayer1Pad;
	PhysicsObject* mPlayer2Pad;
	PhysicsObject* mBall;

	RenderGame* mpRenderWindow;

	chatMessage mChat;
	GameState mGameState;
	broadcastMessage mbroadcast;

	int currentPlayers = 0;
	double mCurrentTime;
	int mCounter = 0;
	bool mStartedGame = false;
	bool mIsCommandLine = false;
};