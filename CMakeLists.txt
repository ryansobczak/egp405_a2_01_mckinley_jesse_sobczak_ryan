cmake_minimum_required(VERSION 2.6)
project(SFML_Game)

SET(CMAKE_CXX_FLAGS_RELEASE "/MDd")
message("CMAKE_CXX_FLAGS_RELEASE is ${CMAKE_CXX_FLAGS_RELEASE}")

set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build) 
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})

include_directories(${CMAKE_SOURCE_DIR}/3rdParty/RakNet/Source)

find_library(RAKNET_LIB
NAMES RakNetLibStatic
HINTS ${CMAKE_SOURCE_DIR}/3rdParty/RakNet)

include_directories(${CMAKE_SOURCE_DIR}/3rdParty/SFML/include)

#debug libs
find_library(SFML_GRAPHICS_DEBUG
	NAMES sfml-graphics.lib
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML/Debug)
find_library(SFML_SYSTEM_DEBUG
	NAMES sfml-system.lib
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML/Debug)
find_library(SFML_WINDOW_DEBUG
	NAMES sfml-window.lib
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML/Debug)

#release libs
find_library(SFML_GRAPHICS_RELEASE
	NAMES sfml-graphics.lib
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML/Release)
find_library(SFML_SYSTEM_RELEASE
	NAMES sfml-system.lib
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML/Release)
find_library(SFML_WINDOW_RELEASE
	NAMES sfml-window.lib
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML/Release)
	
set(RLoc src/render)
set(SLoc src/server)
set(CLoc src/client)
set(TLoc src/test)

add_library(RenderGame ${RLoc}/renderGame.h ${RLoc}/renderGame.cpp ${RLoc}/PhysicsObject.h ${RLoc}/PhysicsObject.cpp 
${RLoc}/PhysicsObjectManager.h ${RLoc}/PhysicsObjectManager.cpp ${RLoc}/CommonData.h)

add_executable(Server ${SLoc}/server.h ${SLoc}/server.cpp ${SLoc}/main.cpp)
add_executable(Client ${CLoc}/client.h ${CLoc}/client.cpp ${CLoc}/main.cpp)
#add_executable(Test ${TLoc}/main.cpp)

target_link_libraries(Server RenderGame debug ${SFML_GRAPHICS_DEBUG} ${SFML_SYSTEM_DEBUG} ${SFML_WINDOW_DEBUG} optimized ${SFML_GRAPHICS_RELEASE} ${SFML_SYSTEM_RELEASE} ${SFML_WINDOW_RELEASE})

target_link_libraries(Client RenderGame debug ${SFML_GRAPHICS_DEBUG} ${SFML_SYSTEM_DEBUG} ${SFML_WINDOW_DEBUG}
										optimized ${SFML_GRAPHICS_RELEASE} ${SFML_SYSTEM_RELEASE} ${SFML_WINDOW_RELEASE})
#target_link_libraries(Test RenderGame debug ${SFML_GRAPHICS_DEBUG} ${SFML_SYSTEM_DEBUG} ${SFML_WINDOW_DEBUG}
										#optimized ${SFML_GRAPHICS_RELEASE} ${SFML_SYSTEM_RELEASE} ${SFML_WINDOW_RELEASE})

target_link_libraries(Server ${RAKNET_LIB} ws2_32.lib)
target_link_libraries(Client ${RAKNET_LIB} ws2_32.lib)
#target_link_libraries(Test ${RAKNET_LIB} ws2_32.lib)

add_custom_command(TARGET Server PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_SOURCE_DIR}/bin $<TARGET_FILE_DIR:Server>)